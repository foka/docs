.. _review:

Review system
=============

We use dedicated conference reviewing software with web front-ends to review
videos after they are recorded. These allow end users to log in and review the
videos online. Typically the review software also manage transcoding the video
files to the desired publishing format and quality. The two systems we recommend
(`veyepar`_ and `SReview`_) handle transcoding. Their respective documentations
are linked here:

* `veyepar setup`_
* `SReview setup`_ -- note that SReview is `packaged in Debian`_

Since it is a bit complicated to set up, if you are planning a Debian event,
`contact us`_ and we might be able to set it up for you.

.. _`veyepar`: https://github.com/CarlFK/veyepar
.. _`SReview`: https://salsa.debian.org/wouter/sreview/tree/master/docs
.. _`veyepar setup`: https://github.com/CarlFK/veyepar/blob/master/README
.. _`SReview setup`: https://salsa.debian.org/wouter/sreview/blob/master/docs/installation.md
.. _`contact us`: contact.html
.. _`packaged in Debian`: https://packages.debian.org/src:sreview
