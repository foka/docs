Volunteer roles
===============

During a DebConf we do not have enough people to carry out all the tasks that
need doing for the whole event. We rely on volunteers to run the equipment in
the rooms while talks are taking place. Some people volunteer for a single talk,
while others volunteer for several.

There are five roles that need fulfilling:

* `Camera Operator`_
* `Coordinator`_
* `Director`_
* `Sound Technician`_
* `Talk Meister`_

Camera Operator
---------------

There are two cameras in the room, each operated by one team member. Generally
speaking, one camera is meant to point at the speaker, and the other at the
audience.  However, these allocations can change if needed. For example, during
the talks, the audience camera can be used as a second shot of the speaker. The
speaker camera can also be pointed to the audience, if there is a discussion
with many people distributed around the room, for instance. When the speaker is
pointing at something, it is useful to provide context by showing what their
hand is pointing at, otherwise, those watching at home will not know where to
look.

A camera should not be moved while it is in action (i.e., while the tally light
is illuminated), as the stream encoding makes movement uncomfortable for the
audience to see. If movement cannot be avoided, it should be slow and smooth.
Any movement should also be communicated to the director. The camera operator
and director should be in visual contact every once in a while so the director
can request specific shots as needed.

Coordinator
-----------

The coordinator is responsible for ensuring that all equipment is working, and
that all necessary volunteers are present, set up, and know how to do their
role. If someone does not arrive in time, then the coordinator must find someone
to replace them, or take their place.


Director
--------

The director controls what is recorded and goes out onto the stream, using
`voctomix`_. It is essential that the director monitors IRC, which is set up on
the Voctomix PC, so that the video team can contact the director about any
issues.

Voctomix offers three ways to display multiple cameras:

The first is to fullscreen the 'A' camera. The fullscreen shot should not be
used continuously for a long time. The shot should change between the speaker,
audience and slides as needed. These changes need to be communicated with the
audience camera operator so that they can get shots of people paying attention.

The second is called Picture in Picture, which renders the 'B' camera over the
bottom right corner of the 'A' camera. This can be used to display the slides
while still allowing the viewer to see the speaker in the bottom corner and
should be done only when the presenter's camera picture does not hide any of the
slide on the screen.

The final method displays the two cameras side-by-side, but this is not a very
useful method as it leaves both too small to see clearly after encoding.

Between talks, and after talks finish for the day, the director should leave
Voctomix on Stream Loop. This displays our `Sponsor Loop`_ on the stream, which
is removed from the published recordings.

.. _voctomix: https://github.com/voc/voctomix
.. _Sponsor Loop: sponsor_loop.html

Sound Technician
----------------

The sound technician operates the audio mixer and is responsible for ensuring
that audio is clear and intelligible on the stream and recordings as well as in
the room itself. The sound technician must know how to operate the audio devices
and mixer, as this role is critical for not only the video streaming and
recording, but also for the talk itself.

The channels on the mixer are labelled, indicating to what device each one
corresponds. The common audio inputs are the wireless and headset mics, the
speaker's computer, and the ambient mics (to get the room sound). The outputs
include the streaming/recording and the room speakers, which are controlled by
sliders on the right side of the mixer.

The headset and wireless microphones use disposable batteries (9V or AA,
depending on the type), so care must be taken that these are replaced regularly
(once a day to once every second day, depending on how much use they have had).
The headset mic must be placed on the speaker's head carefully. It should be
placed on the same level as the speaker's mouth, not so far away that it does
not pick the speaker up clearly, but not so close that it picks up the speaker's
breathing.

Talk Meister
------------

The talk meister introduces the speaker, keeps track of the time and coordinates
the question and answer sessions. At the end of the talk they also thank the
speaker. Often the coordinator and the talk meister are the same person when
there are not many volunteers.

Speaker introductions should include a small amount of background information.
Talk meisters should also remind audience members to wait for the mic before
asking a question, as questions asked without the mic will not be heard on the
stream and recording.
