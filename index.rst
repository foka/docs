DebConf videoteam documentation
-------------------------------

.. _index:

.. toctree::
   :maxdepth: 2

   getting_started.rst
   advice_for_presenters.rst
   hardware.rst
   ansible.rst
   streaming.rst
   review.rst
   venue_checklist.rst
   room_setup.rst
   opsis.rst
   sponsor_loop.rst
   video_archive.rst
   volunteer_roles.rst
   contact.rst
