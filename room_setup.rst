Room setup
==========

This section describes the typical layout of a talk room at a DebConf. It
includes the `video`_ and `audio`_ components.

Video
-----

|room-setup-video|

The video hardware we use is described in the `video hardware section`_. There
are two cameras in every room and an `Opsis`_ for capturing the presenter's
presentation. These inputs are mixed together using Voctomix. The Opsis output
is streamed over the wired network by the capture PC to Voctomix. The output
from Voctomix is streamed out to the stream servers and recorded locally for
upload to the review system.

.. |room-setup-video| image:: /_static/room_setup_video.svg
.. _`video hardware section`: hardware.html#video cameras
.. _`Opsis`: opsis.html

Audio
-----

|room-setup-audio|

The audio setup described here uses the equipment listed in the `hardware section`_. It uses:

* two headset mics for presenters
* two handheld mics for questions
* two omni-directional mics for ambient noise
* one stereo DI for the presenter's laptop audio

Ambient noise is the clapping and general buzz that happens in the background
during a talk. This is useful for the stream, as it makes viewers feel as though
they are in the room. These two mics are not sent to the room audio system. The
presenter's laptop audio is captured to ensure that any audio or video played
during the talk can also be played on the stream and PA.

There are two audio mixes which need to be somewhat separate - one for the PA
and one for the stream. Most desks can support this using an aux bus or
sub-groups. Using aux buses requires 2 post-fader buses (left and right). This
is so the channel fader affects both the PA and the stream levels. The main-mix,
or one of the sub-groups, is run to the camera for the stream and recording.
This way, the camera handles synchronizing the audio and video. The aux bus or
other sub-group pair is run to the room's speakers.

.. |room-setup-audio| image:: /_static/room_setup_audio.svg
.. _`hardware section`: hardware.html#audio

Combined
--------

Combined, this looks like this:

|room-setup|

.. |room-setup| image:: /_static/room_setup.svg

